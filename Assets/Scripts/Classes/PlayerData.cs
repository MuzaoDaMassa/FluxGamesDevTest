using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;

[System.Serializable]
public class PlayerData 
{
    public int _currentLevel;
    public int _score;

    public PlayerData ()
    {
        _currentLevel = Statics.CurrentLevel;
        _score = Statics.Points;

    }
}
