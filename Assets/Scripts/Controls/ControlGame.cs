﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    public GameObject _pauseScreen;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;
    
    // Use this for initialization
    void Start () {
        Statics.EnemiesDead = 0;
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        
    }

    private void Update()
    {
        PauseGame();
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);
    }

    public void LevelPassed()
    {
        
        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;
        if (Statics.CurrentLevel < 3)
        {
            SavePlayerData();
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        SavePlayerData();
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    // Pauses game and audio source while destroying active shots and disabling enemies images
    private void PauseGame()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            _pauseScreen.SetActive(true);
            GetComponent<AudioSource>().Pause();
            Time.timeScale = 0.0f;

            GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject ini in Enemies)
            {
                ini.GetComponent<Image>().enabled = false;
            }

            GameObject[] Shots = GameObject.FindGameObjectsWithTag("Shot");
            foreach (GameObject s in Shots)
            {
                Destroy(s);
            }
        }
    }

    // Unpause game and audio source while enabling enemies images
    public void UnpauseGame()
    {
        GetComponent<AudioSource>().UnPause();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            ini.GetComponent<Image>().enabled = true;
        }
        Time.timeScale = 1.0f;
        _pauseScreen.SetActive(false);
    }

    // Saves the current level variable so player can continue where he left off
    private void SavePlayerData()
    {
        SaveSystem.SavePlayer();
    }
}
