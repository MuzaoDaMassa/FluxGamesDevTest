using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Gaminho;

public class LoadPlayerData : MonoBehaviour
{
    public void LoadPlayer()
    {
        PlayerData _data =  SaveSystem.LoadPlayer();

        Statics.CurrentLevel = _data._currentLevel;
        Statics.Points = _data._score;

        SceneManager.LoadScene("Game");
    }
}
